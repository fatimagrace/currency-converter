import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Config } from '../utils/Config';

import { CurrencyOptions } from '../interfaces/currency-options';

@Injectable({
    providedIn: 'root'
})
export class ConversionData {

    private config: Config = new Config();

    constructor(public http: HttpClient) { }

    getRequestUrl(endpoint: string, params: any = {}){
        params['access_key'] = this.config.getAccessKey();
        
        var queryStr = Object.keys(params).map(key => key + '=' + params[key]).join('&');
        return this.config.baseUrl() + endpoint + "?" + queryStr;
    }

    getCurrencySymbols(){
        return this.http
            .get(this.getRequestUrl("symbols"))
            .pipe(map((data:any) => { return data } , this)); 
    }

    getConversion(currency: CurrencyOptions){
        var params = { symbols: currency.from + "," + currency.to };
        return this.http
            .get(this.getRequestUrl("latest", params))
            .pipe(map((data:any) => { 

                var rates = data.rates;
                var convertedAmount = ((1 / rates[currency.from]) * rates[currency.to]) * currency.amount;

                return convertedAmount;

            } , this)); 
    }

}
