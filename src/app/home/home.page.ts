import { Component } from '@angular/core';
import { CurrencyOptions } from '../interfaces/currency-options';
import { ConversionData } from '../providers/conversion-data';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private symbols: any = {};
  currency: CurrencyOptions = { from: '', to: '', amount: 0 };
  result: number = 0;

  constructor(private conversionData: ConversionData) {

    this.conversionData.getCurrencySymbols().subscribe( (data) => {
      this.symbols = data.symbols;
    });
  }

  convert(){
    this.conversionData.getConversion(this.currency).subscribe( data => this.result = data);
  }

  onFieldChange(){
    if(this.currency.amount == 0 || this.currency.from == '' || this.currency.to == '') return;
    
    this.convert();
  }

}
