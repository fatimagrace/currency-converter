export interface CurrencyOptions {
    from: string;
    to: string;
    amount: number;
}
  