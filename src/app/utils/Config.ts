import { Injectable } from '@angular/core';

enum Environment { DEV, PROD }

const BASE_URL = {
    DEV: "http://data.fixer.io/api/",
    PROD: "http://data.fixer.io/api/"
};

const API_KEY = "8904e696a2c3bf9d345dee010908b4ce";

/**
 * This class handles all the network configurations for the application
 **/
@Injectable()
export class Config {

    // Indicates environment the app is running. 
    // Initial or default value is Environment.DEV
    environment = Environment.DEV;

    /**
     * @return {str} Returns the base url of the current environment
     **/
    baseUrl(){
        switch (this.environment) {
            case Environment.PROD: return BASE_URL.PROD;
            default: return BASE_URL.DEV;
        }
    }

    getAccessKey() { return API_KEY; }
}